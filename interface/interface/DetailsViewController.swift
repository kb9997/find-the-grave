//
//  DetailsViewController.swift
//  interface
//
//  Created by macKawa on 12.03.2019.
//  Copyright © 2019 macKawa. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import CoreLocation

protocol DetailsViewControllerDelegate{
    func myVCDidFinish(controller: DetailsViewController, text: String)
}
class Annotation: NSObject, MKAnnotation{
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(name: String, coordinate: CLLocationCoordinate2D){
        self.title = name
        self.coordinate = coordinate
        super.init()
    }
}
class DetailsViewController: UIViewController {

    var delegate:DetailsViewControllerDelegate? = nil
    var dead: Dead?
    var deadObject: NSManagedObject?
    
    var latitude: Float = 54.169531
    var longitude: Float = 16.207692
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bornDateLabel: UILabel!
    @IBOutlet weak var deathDateLabel: UILabel!
    @IBAction func editButton(_ sender: Any) {
        //performSegue(withIdentifier: "detailsToEditSegue", sender: nil)
    }
    @IBOutlet weak var imageOutlet: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    
    
let myLocationManager = CLLocationManager()
    
    override func viewWillAppear(_ animated: Bool) {
        checkLocationServices()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
        imageOutlet.isUserInteractionEnabled = true
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let tempDeath:String? = formatter.string(from: deadObject?.value(forKeyPath: "deathDate") as? Date ?? Date())
        let tempBirth:String? = formatter.string(from: deadObject?.value(forKeyPath: "birthDate") as? Date ?? Date())
        
        latitude = deadObject?.value(forKeyPath: "latitude") as? Float ?? 0.0
        longitude = deadObject?.value(forKeyPath: "longitude") as? Float ?? 0.0
        if let data = deadObject?.value(forKey: "image") as? Data{
            imageOutlet.image = UIImage(data: data)
        }else{
            imageOutlet.image = UIImage(named: "cGrave")
        }
        nameLabel.text = deadObject?.value(forKey: "name") as? String ?? "placeholder"
        bornDateLabel.text = tempBirth ?? "placeholder"
        deathDateLabel.text = tempDeath ?? "placeholder"
        
        checkLocationServices()

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "detailsToEditSegue"){
            if let destVC = segue.destination as? EditViewController{

                let deadToSend = self.deadObject
                destVC.deadObject = deadToSend
                
                //destVC.delegate = self

            }
        }
    }
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            let location = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
            let annotation = Annotation(name: deadObject?.value(forKey: "name") as? String ?? "Placeholder", coordinate: location.coordinate)
            mapView.addAnnotation(annotation)
            mapView.setCenter(location.coordinate, animated: true)
            
            break
        case .denied:
            break
        case .notDetermined:
            myLocationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        case .authorizedAlways:
            break
        }
    }
    func setupLocationManager(){
        myLocationManager.delegate = self
        myLocationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAuthorization()
        }else{
            
        }
    }
    
    var imagePreview:Bool = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first!
        
        if imagePreview == false {
            if touch.view == imageOutlet{
            UIView.animate(withDuration: 1, animations: {
                
                let tempScaleX = UIScreen.main.bounds.width / self.imageOutlet.frame.width
                let tempTranslationY = self.imageOutlet.frame.midY - UIScreen.main.bounds.height/2
                let tempTranslationX = (self.imageOutlet.superview?.frame.width ?? 320)/2 - self.imageOutlet.frame.midY
                let moveTransform = CGAffineTransform(translationX: tempTranslationX, y: -tempTranslationY)
                let moveAndScale = moveTransform.scaledBy(x: tempScaleX, y: tempScaleX)
                self.imageOutlet.transform = moveAndScale
                
                print("Preview: true")
                self.imagePreview = true
                self.mapView.isUserInteractionEnabled = false
            })
                
            }else{
                
            }
            
        }else{
            UIView.animate(withDuration: 1, animations: {
                self.imageOutlet.transform = CGAffineTransform.identity
            })
            print("Preview: false")
            self.mapView.isUserInteractionEnabled = true

            self.imagePreview = false
        }
        

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DetailsViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //
    }
}

extension DetailsViewController{
    func hideKeyboardWhenTappedAround(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailsViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
}
