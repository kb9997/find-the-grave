//
//  Formatter.swift
//  interface
//
//  Created by macKawa on 23.03.2019.
//  Copyright © 2019 macKawa. All rights reserved.
//

import Foundation

class MyFormatter{
    
    let dateFormatter:DateFormatter = DateFormatter()
    
    func formatDate(fromString: String?) -> Date{
        
        return dateFormatter.date(from: fromString ?? "01-01-1900") ?? Date()
        
    }
    func formatDate(fromDate: Date?) -> String{
        return dateFormatter.string(from: fromDate ?? Date())
    }
    init(){
        dateFormatter.dateFormat = "dd-MM-yyyy"
    }
    
}
