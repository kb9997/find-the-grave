//
//  ViewController.swift
//  interface
//
//  Created by macKawa on 12.03.2019.
//  Copyright © 2019 macKawa. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate, DetailsViewControllerDelegate {
    
    var deadTab: [NSManagedObject] = []
    var currentTab: [NSManagedObject] = []

    var searchController: UISearchController!
    
    @IBAction func unwindToSearchView(segue: UIStoryboardSegue){
    }
    @IBOutlet weak var table: UITableView!
    
    @IBAction func addButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "searchToAddSegue", sender: nil)
    }
    
    func save(name: String){
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "DeadPerson", in: managedContext)!
        
        let deadPerson = NSManagedObject(entity: entity, insertInto: managedContext)
        
        deadPerson.setValue(name, forKey: "name")
        deadPerson.setValue(Date(), forKey: "birthDate")
        deadPerson.setValue(Date(), forKey: "deathDate")
        
        do{
            try managedContext.save()
            deadTab.append(deadPerson)
        } catch let error as NSError{
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DeadPerson")
        
        do{
            deadTab = try managedContext.fetch(fetchRequest)
        }catch let error as NSError{
            print("Could not fetch data. \(error) \(error.userInfo)")
        }
        table.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        table.reloadData()
        currentTab = deadTab
        table.delegate = self
        table.dataSource = self
        
        //self.view.backgroundColor = UIColor(red: 0.19, green: 0.22, blue: 0.29, alpha: 1.0)
        self.view.backgroundColor = UIColor(red: 0.10, green: 0.08, blue: 0.14, alpha: 1.0)
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.searchBar.barStyle = UIBarStyle.black
        searchController.searchBar.tintColor = UIColor.white
        //searchController.searchBar.barTintColor = UIColor(red: 0.19, green: 0.22, blue: 0.29, alpha: 1.0)
        //searchController.searchBar.barTintColor = UIColor(red: 0.34, green: 0.20, blue: 0.43, alpha: 1.0)

        
        searchController.searchBar.scopeButtonTitles = ["All", "Family", "Co-work", "Friend", "Other"]
        searchController.searchBar.delegate = self
        
        
        
        let navImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 40))
        navImageView.contentMode = .scaleAspectFit
        navImageView.image = UIImage(named: "logo")
        self.navigationItem.titleView = navImageView
        self.navigationItem.searchController = searchController
        table.backgroundView = UIView()
//        table.backgroundView!.backgroundColor = UIColor(red: 0.19, green: 0.22, blue: 0.29, alpha: 1.0)
        //table.backgroundView!.backgroundColor = UIColor(red: 0.19, green: 0.11, blue: 0.25, alpha: 1.0)
        //table.backgroundView!.backgroundColor = UIColor(red: 0.13, green: 0.08, blue: 0.13, alpha: 1.0)
        table.backgroundView!.backgroundColor = UIColor(red: 0.10, green: 0.08, blue: 0.14, alpha: 1.0)
        definesPresentationContext = true
        
        
    }
   
    
    func searchBarIsEmpty() -> Bool{
        return searchController.searchBar.text?.isEmpty ?? true
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        self.searchController.searchBar.showsCancelButton = true
    }
    func isFiltering() -> Bool {
        let searchBarScopeIsFilltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFilltering)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        //table.reloadData()
    }
    func updateSearchResults(for searchController: UISearchController) {
        let scope = searchController.searchBar.scopeButtonTitles![searchController.searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchController.searchBar.text!, scope: searchController.searchBar.scopeButtonTitles! [selectedScope])
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All"){
        currentTab = deadTab.filter({(dead : NSManagedObject) -> Bool in
            
            let tempName: String = dead.value(forKeyPath: "name") as? String ?? "place_holder_name"
            let tempCategory = dead.value(forKeyPath: "category") as? Int16 ?? 5
            
            var categoryMatch = (scope == "All") || ("Other" == scope)
            
            switch tempCategory {
            case 1:
                categoryMatch = (scope == "All") || ("Family" == scope)
                break
            case 2:
                categoryMatch = (scope == "All") || ("Friend" == scope)
                break
            case 3:
                categoryMatch = (scope == "All") || ("Colleague" == scope)
                break
            case 4:
                categoryMatch = (scope == "All") || ("Co-work" == scope)
                break
            case 5:
                categoryMatch = (scope == "All") || ("Other" == scope)
                break
            default:
                categoryMatch = (scope == "All") || ("Other" == scope)
            }
            
            if (searchBarIsEmpty()) {
                return categoryMatch
            }else{
                return categoryMatch && tempName.lowercased().contains(searchText.lowercased())
            }
//            let tempName: String = dead.value(forKeyPath: "name") as? String ?? "place_holder_name"
//            return tempName.lowercased().contains(searchText.lowercased())
        })
        table.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return currentTab.count
        }
        return deadTab.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let dead: NSManagedObject
        
        if isFiltering(){
            dead = currentTab[indexPath.row]
        }else{
            dead = deadTab[indexPath.row]
        }
            cell.textLabel?.text = dead.value(forKeyPath: "name") as? String
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let tempDate:String = formatter.string(from: dead.value(forKeyPath: "deathDate") as? Date ?? Date() )
            let tempCategory = dead.value(forKeyPath: "category") as? Int16 ?? 5
        
        
            cell.detailTextLabel?.text = tempDate
        
        switch tempCategory {
        case 1:
            cell.imageView!.image = UIImage(named: "rGrave")
            break
        case 2:
            cell.imageView!.image = UIImage(named: "gGrave")
            break
        case 3:
            cell.imageView!.image = UIImage(named: "bGrave")
            break
        case 4:
            cell.imageView!.image = UIImage(named: "yGrave")
            break
        case 5:
            cell.imageView!.image = UIImage(named: "wGrave")
            break
        default:
            cell.imageView!.image = UIImage(named: "wGrave")
        }
        
        cell.imageView?.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        let itemSize = CGSize.init(width: 30, height: 30)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
//            switch tempCategory {
//            case "Family":
//                cell.imageView!.image = UIImage(named: "rGrave")
//                break
//            case "Friend":
//                cell.imageView!.image = UIImage(named: "gGrave")
//                break
//            case "Colleague":
//                cell.imageView!.image = UIImage(named: "bGrave")
//                break
//            case "Co-worker":
//                cell.imageView!.image = UIImage(named: "yGrave")
//                break
//            case "Other":
//                cell.imageView!.image = UIImage(named: "wGrave")
//                break
//            default:
//                cell.imageView!.image = UIImage(named: "wGrave")
//            }
        
            return cell
    }
    
    func myVCDidFinish(controller: DetailsViewController, text: String) {
        controller.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "detailsSegue"){
            if let destVC = segue.destination as? DetailsViewController{
                let index = table.indexPathForSelectedRow!.row
                let dead = deadTab[index]
                //destVC.dead = Dead.init(name: "placeholder \(index)", dateOfBirth: Date(), dateOfDeath: Date())
                
                let deathDate = dead.value(forKeyPath: "deathDate") as? Date ?? Date()
                let birthDate = dead.value(forKeyPath: "birthDate") as? Date ?? Date()
                destVC.dead = Dead.init(name: dead.value(forKeyPath: "name") as? String ?? "place-holder", dateOfBirth: deathDate, dateOfDeath: birthDate)
                destVC.deadObject = deadTab[index]
                destVC.delegate = self
                
            }
        }
    }
}

extension ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailsSegue", sender: nil)
    }
}

extension UIView {
    func traverseSubviewForViewOfKind(kind: AnyClass ) -> UIView?{
        var matchingView: UIView?
        
        for aSubview in subviews{
            if type(of: aSubview) == kind{
                matchingView = aSubview
                return matchingView
            }else{
                if let matchingView = aSubview.traverseSubviewForViewOfKind(kind: kind){
                    return matchingView
                }
            }
        }
        return matchingView
    }
}
