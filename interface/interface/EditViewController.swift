//
//  EditViewController.swift
//  interface
//
//  Created by macKawa on 21.03.2019.
//  Copyright © 2019 macKawa. All rights reserved.
//

import UIKit
import CoreData
import MapKit
class EditViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
        
    }
    
    var categoryPicker: UIPickerView = UIPickerView()
    var selectedCategory: String? = "Other"
    let categories = ["Family","Friend","Co-worker","Other"]
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCategory = categories[row]
        switch selectedCategory {
        case "Family":
            categoryImage.image = UIImage(named: "rGrave")
            break
        case "Co-worker":
            categoryImage.image = UIImage(named: "yGrave")
            break
        case "Friend":
            categoryImage.image = UIImage(named: "gGrave")
            break
        case "Colleague":
            categoryImage.image = UIImage(named: "bGrave")
            break
        case "Other":
            categoryImage.image = UIImage(named: "wGrave")
            break
        default:
            categoryImage.image = UIImage(named: "wGrave")
        }
    }
    var deadObject: NSManagedObject?
    var deadNameForEdit: String?
    var latitudeTemp: Float? = 0.000000
    var longitudeTemp: Float? = 0.000000
    weak var activeDateTextField: UITextField?
    let datePickerView:UIDatePicker = UIDatePicker()
    let globalFormatter:MyFormatter = MyFormatter()
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var birthTextField: UITextField!
    @IBOutlet weak var deathTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBAction func birthPicker(_ sender: UITextField) {
        sender.inputView = datePickerView
        datePickerView.date = globalFormatter.formatDate(fromString: birthTextField.text)
        activeDateTextField = birthTextField
    }
    @IBAction func deathPicker(_ sender: UITextField) {
        sender.inputView = datePickerView
        datePickerView.date = globalFormatter.formatDate(fromString: deathTextField.text)
        activeDateTextField = deathTextField
    }
    @IBAction func setLocalizationButton(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Location", message: "Choose a method to set the location",preferredStyle: .actionSheet)
        let coordinatesAction = UIAlertAction(title: "Co-ordinates", style: .default){ (action: UIAlertAction) in
            let coordinatesSheet = UIAlertController(title: "Co-ordiantes", message: "Input latitude and longitude", preferredStyle: .alert)
            let save = UIAlertAction(title: "Select", style: .default, handler: { (UIAlertAction) in
                // zaisujemy !!
                //self.locationTextField.text =  "\(coordinatesSheet.textFields![0].text ?? "00.000000" ) , \(coordinatesSheet.textFields![1].text ?? "00.000000")"
                self.latitudeTemp = (coordinatesSheet.textFields![0].text as NSString?)?.floatValue
                self.longitudeTemp = (coordinatesSheet.textFields![1].text as NSString?)?.floatValue
                self.locationTextField.text = "\(self.latitudeTemp ?? 00.000000) , \(self.longitudeTemp ?? 00.000000)"
                
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            coordinatesSheet.addTextField{ (textField1) in
                textField1.placeholder = "Latitude"
                textField1.keyboardType = UIKeyboardType.numbersAndPunctuation
                textField1.textContentType = UITextContentType.location
                textField1.text = "\(self.latitudeTemp ?? 0.0)"
            }
            coordinatesSheet.addTextField{ (textField2) in
                textField2.placeholder = "Longitude"
                textField2.keyboardType = UIKeyboardType.numbersAndPunctuation
                textField2.textContentType = UITextContentType.location
                textField2.text = "\(self.longitudeTemp ?? 0.0)"
            }
            coordinatesSheet.addAction(save)
            coordinatesSheet.addAction(cancelAction)
            self.present(coordinatesSheet, animated: true)
        
            }
            let mapAction = UIAlertAction(title: "Map", style: .default) { (action: UIAlertAction) in
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                
                let height = self.view.safeAreaLayoutGuide.layoutFrame.minY
                
                self.mapPicker = UIView(frame: CGRect.init(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
                self.mapPickerToolBar = UIToolbar(frame: CGRect.init(x: 0.0, y: self.mapPicker.frame.minY, width: UIScreen.main.bounds.size.width, height: height + 50))
                self.mapPickerToolBar.barStyle = .blackTranslucent
                self.mapPickerToolBar.items = [UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.mapPickerDoneButtonTapped))]
                self.mapView = MKMapView(frame: self.mapPicker.frame)
                self.mapView.showsUserLocation = true
                self.mapView.setCenter(CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: self.latitudeTemp ?? 0.0) ?? 0.0 , longitude: CLLocationDegrees(exactly: self.longitudeTemp ?? 0.0) ?? 0.0), animated: true)
                let pin = UIImageView(frame: CGRect.init(x: self.mapView.frame.width/2-15, y: (self.mapView.frame.height/2)-15, width: 30, height: 30))
                pin.image = UIImage(named: "pin")
                
                self.mapPicker.addSubview(self.mapView)
                self.mapPicker.addSubview(pin)
                self.view.addSubview(self.mapPicker)
                self.view.addSubview(self.mapPickerToolBar)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
            actionSheet.addAction(coordinatesAction)
            actionSheet.addAction(mapAction)
            actionSheet.addAction(cancelAction)
        
            present(actionSheet, animated: true)
    }
    
    @objc func mapPickerDoneButtonTapped(){
        mapPickerToolBar.removeFromSuperview()
        mapPicker.removeFromSuperview()
        let lati = NSNumber(value: mapView.centerCoordinate.latitude)
        latitudeTemp = lati.floatValue
        let longi = NSNumber(value: mapView.centerCoordinate.longitude)
        longitudeTemp = longi.floatValue
        locationTextField.text = "\(latitudeTemp ?? 00.000000) , \(longitudeTemp ?? 00.000000)"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    var mapView = MKMapView()
    var mapPicker = UIView()
    var mapPickerToolBar = UIToolbar()
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBAction func setCategoryButton(_ sender: Any) {
        categoryPicker = UIPickerView.init()
        categoryPicker.delegate = self
        categoryPicker.backgroundColor = .gray
        categoryPicker.setValue(UIColor.black, forKey: "textColor")
        categoryPicker.autoresizingMask = .flexibleWidth
        categoryPicker.contentMode = .center
        
        let tempY = UIScreen.main.bounds.height*0.6
        categoryPicker.frame = CGRect(x: 0.0, y: tempY, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height - tempY)
        
        view.addSubview(categoryPicker)
        categoryPicker.selectRow(4, inComponent: 0, animated: true)
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: categoryPicker.frame.minY, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .black
        toolBar.tintColor = UIColor(red: 0.10, green: 0.08, blue: 0.14, alpha: 1.0)
        toolBar.items = [UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonTapped))]
        self.view.addSubview(toolBar)
    }
    @objc func doneButtonTapped(){
        toolBar.removeFromSuperview()
        categoryPicker.removeFromSuperview()
    }
    
    var toolBar = UIToolbar()
    @IBOutlet weak var imageOutlet: UIImageView!
    @IBAction func loadImageButton(_ sender: Any) {
        
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose source of photo", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Camera not available", message: "",preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                alert.addAction(cancelAction)
                self.present(alert, animated: true)
                
            }
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imageOutlet.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveButton(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Save", message: "Save changes?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default){
            [unowned self] action in
            self.updateData()
            self.performSegue(withIdentifier: "unwindSegueToSearchView", sender: self)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
        
    }
    @IBAction func deleteButton(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Delete", message: "Delete this record?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default){
            [unowned self] action in
            self.deleteData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
        
    }
    @objc func datePickerValueChanged(sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        activeDateTextField!.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func imageOutletTouched(_ recognizer: UITapGestureRecognizer){
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        if imageOutletSize == 1 {
            imageOutlet.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
            imageOutletSize = 2
        }else{
            imageOutlet.frame = imageOutletOriginalSize ?? CGRect(x: 0, y: 0, width: 20, height: 20)
            imageOutletSize = 1
        }
        
    }

    var imagePreview:Bool = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first!
        if touch.view == imageOutlet{
            if imagePreview == false {
                UIView.animate(withDuration: 1, animations: {
                    //self.imageOutlet.transform = CGAffineTransform(scaleX: 2, y: 2)
                    
                    let tempScaleX = UIScreen.main.bounds.width / self.imageOutlet.frame.width
                    let tempTranslationY = self.imageOutlet.frame.midY - UIScreen.main.bounds.height/2
                    let moveTransform = CGAffineTransform(translationX: 0, y: -tempTranslationY)
                    let moveAndScale = moveTransform.scaledBy(x: tempScaleX, y: tempScaleX)
                    self.imageOutlet.transform = moveAndScale
                
                    self.imagePreview = true
            })
            }else{
                UIView.animate(withDuration: 1, animations: {
                        self.imageOutlet.transform = CGAffineTransform.identity
                    })
                self.imagePreview = false
            }
        }else{
            view.endEditing(true)
            view.willRemoveSubview(categoryPicker)
            categoryPicker.endEditing(true)
        }
    }
    var imageOutletSize: Int = 1
    var imageOutletOriginalSize:CGRect?

    override func viewDidLoad() {
        super.viewDidLoad()
        deadNameForEdit = deadObject?.value(forKeyPath: "name") as? String ?? "placehodler"
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        if let data = deadObject?.value(forKey: "image") as? Data{
            imageOutlet.image = UIImage(data: data)
        }else{
            imageOutlet.image = UIImage(named: "cGrave")
        }
        imageOutlet.isUserInteractionEnabled = true
        imageOutlet.superview?.bringSubviewToFront(imageOutlet)
        imageOutletOriginalSize = imageOutlet.frame
        let tempBirth:String? = globalFormatter.formatDate(fromDate: deadObject?.value(forKeyPath: "birthDate") as? Date ?? Date())
        let tempDeath:String? = globalFormatter.formatDate(fromDate: deadObject?.value(forKeyPath: "deathDate") as? Date ?? Date())
        latitudeTemp = deadObject?.value(forKeyPath: "latitude") as? Float ?? 0.0
        longitudeTemp = deadObject?.value(forKeyPath: "longitude") as? Float ?? 0.0
        let tempCategory = deadObject?.value(forKeyPath: "category") as? Int16 ?? 4
        switch tempCategory {
        case 1:
            selectedCategory = "Family"
            categoryPicker.selectRow(0, inComponent: 0, animated: true)
            categoryImage.image = UIImage(named: "rGrave")
            break
        case 2:
            selectedCategory = "Friend"
            categoryPicker.selectRow(1, inComponent: 0, animated: true)
            categoryImage.image = UIImage(named: "gGrave")
            break
        case 3:
             selectedCategory = "Colleague"
             categoryPicker.selectRow(2, inComponent: 0, animated: true)
             categoryImage.image = UIImage(named: "bGrave")
            break
        case 4:
             selectedCategory = "Co-worker"
             categoryPicker.selectRow(3, inComponent: 0, animated: true)
             categoryImage.image = UIImage(named: "yGrave")
            break
        case 5:
             selectedCategory = "Other"
             categoryPicker.selectRow(4, inComponent: 0, animated: true)
             categoryImage.image = UIImage(named: "wGrave")
            break
        default:
             selectedCategory = "Other"
             categoryPicker.selectRow(4, inComponent: 0, animated: true)
             categoryImage.image = UIImage(named: "wGrave")
        }
        
        nameTextField.text = deadNameForEdit
        deathTextField.text = tempDeath
        birthTextField.text = tempBirth
        locationTextField.text = "\(latitudeTemp ?? 00.000000) , \(longitudeTemp ?? 00.000000)"
        
        
    }
    
    func updateData(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "DeadPerson")
        guard let name = deadNameForEdit else{
            return
        }
        fetchRequest.predicate = NSPredicate(format: "name = %@", name)
        do{
            let test = try managedContext.fetch(fetchRequest)
            let objectUpdate = test[0] as! NSManagedObject

            let birthDateTemp = globalFormatter.formatDate(fromString: birthTextField.text)
            let deathDateTemp = globalFormatter.formatDate(fromString: deathTextField.text)
            
            
            objectUpdate.setValue(nameTextField.text, forKey: "name")
            objectUpdate.setValue(deathDateTemp, forKey: "deathDate")
            objectUpdate.setValue(birthDateTemp, forKey: "birthDate")
            objectUpdate.setValue(latitudeTemp, forKey: "latitude")
            objectUpdate.setValue(longitudeTemp, forKey: "longitude")
            
            let tempImage = imageOutlet.image?.pngData() as NSData?
            objectUpdate.setValue(tempImage, forKey: "image")
            
            switch selectedCategory {
            case "Family":
                objectUpdate.setValue(1, forKey: "category")
                break
            case "Friend":
                objectUpdate.setValue(2, forKey: "category")
                break
            case "Colleague":
                objectUpdate.setValue(3, forKey: "category")
                break
            case "Co-worker":
                objectUpdate.setValue(4, forKey: "category")
                break
            case "Other":
                objectUpdate.setValue(5, forKey: "category")

                break
            default:
                objectUpdate.setValue(5, forKey: "category")
            }
            
            do{
                try managedContext.save()
            }catch{
                print("Error: \(error)")
            }
        }catch{
            print("Error: \(error)")
        }
    }
    
    func deleteData(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DeadPerson")
        
        guard let name = deadNameForEdit else{
            return
        }
        fetchRequest.predicate = NSPredicate(format: "name = %@", name)
        
        do{
            let test = try managedContext.fetch(fetchRequest)
            
            let objectToDelete = test[0]
            managedContext.delete(objectToDelete)
            
            do{
                try managedContext.save()
            }catch let error as NSError{
                print("Could not save data. \(error) \(error.userInfo)")
            }
            
        }catch let error as NSError{
            print("Could not fetch data. \(error) \(error.userInfo)")
        }
        performSegue(withIdentifier: "unwindSegueToSearchView", sender: self)
    }
    


}
