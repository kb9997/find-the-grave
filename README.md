# Find the grave 

Simple app to save location of graves.

## Features

User can save:
name,
 date of birth,
 date of death, 
 location,
 category (family, co-worker etc.),
 photo.

## Info

App created for IPhones only.
Tested on IPhone SE with iOS 12.2